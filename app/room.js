// should contain
// monster and items location info
function Room(el, size) {
  this.el = el;
  this.size = {x: 19, y: 9};
  this.monsters = [];
  this.items = [];
  this.doors = [];
  this.env_items = [];
  this.blockingMap = fillArray(this.size.x, this.size.y, 0);
}

Room.prototype.addDoors = function() {
  var door = new DoorItem(create('div'), new Room(this.el));
  door.position = {x: 10, y: 4};
  this.markSoftBlocked(door.position.x, door.position.y);
  this.doors.push(door);
}

//fill room with stuff
Room.prototype.populate = function() {
  // monsters
  for (var i=0; i<rnd(1,5); i++) {
    var m = new Monster(create('div'), 'monster monster' + rnd(1, 3));
    var pos_x, pos_y;
    for (;;) {
      pos_x = rnd(2, this.size.x - 2);
      pos_y = rnd(2, this.size.y - 2);
      if (this.isEmpty(pos_x, pos_y)) break;
    }
    m.position = {x: pos_x, y: pos_y};
    this.markBlocked(pos_x, pos_y);
    this.monsters.push(m);
  }
  // env items
  for (var i=0; i<rnd(1,5); i++) {
    var m = new EnvironmentalItem(create('div'), 'environment-item');
    var pos_x, pos_y;
    for (;;) {
      pos_x = rnd(2, this.size.x - 2);
      pos_y = rnd(2, this.size.y - 2);
      if (this.isEmpty(pos_x, pos_y)) break;
    }
    m.position = {x: pos_x, y: pos_y};
    this.markBlocked(pos_x, pos_y);
    this.env_items.push(m);
  }
  // items
  for (var i=0; i<rnd(1,5); i++) {
    var m = new Item(create('div'), 'item' + rnd(1, 3));
    var pos_x, pos_y;
    for (;;) {
      pos_x = rnd(1, this.size.x - 2);
      pos_y = rnd(1, this.size.y - 2);
      if (this.isEmpty(pos_x, pos_y)) break;
    }
    m.position = {x: pos_x, y: pos_y};
    this.markSoftBlocked(pos_x, pos_y);
    this.items.push(m);
  }
}

Room.prototype.find = function(what, pos_x, pos_y) {
  var targetArray = this[what];
  for (var i=0; i<targetArray.length; i++) {
    if (targetArray[i].position.x == pos_x && targetArray[i].position.y == pos_y)
      return targetArray[i];
  }
  return undefined;
}

Room.prototype.markBlocked = function(x, y) {
  this.blockingMap[y][x] = 1;
}

Room.prototype.markSoftBlocked = function(x, y) {
  this.blockingMap[y][x] = 2;
}

Room.prototype.markUnblocked = function(x, y) {
  this.blockingMap[y][x] = 0;
}

Room.prototype.isEmpty = function(x, y) {
  return this.blockingMap[y][x] === 0;
}

Room.prototype.notBlocked = function(x, y) {
  return this.blockingMap[y][x] !== 1;
}

Room.prototype.setSize = function(x, y) {
  this.size.x = x;
  this.size.y = y;
}

// render different maps
Room.prototype.render = function(t) {
  for (var i=0; i<this.size.y; i++) {
    for (var j=0; j<this.size.x; j++) {
      var el = create('div');
      if (i === 0 || i === this.size.y - 1) {
        el.className = 'tile wall'
      } else if (j === 0 || j === this.size.x -1) {
        el.className = 'tile wall'
      } else {
        el.className = 'tile floor2';
      }
      el.style.top = i * SETTINGS.CELL_SIZE + "px";
      el.style.left = j * SETTINGS.CELL_SIZE + "px";
      this.el.appendChild(el);
    }
  }

}
