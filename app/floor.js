function Floor(level) {
  this.level = level;
  this.size = 10;
  this.map = fillArray(10, 10, 0);
  this.monsters = [];
  // seed map with obstacles and monsters
  for (var i=0; i<this.level; i++) {
    var x, y;
    x = rnd(2, 7);
    y = rnd(1, 8);
    var directions = ["left", "top"]
    var direction = directions[rnd(0, 1)];
    var className = 'monster ';
    if (direction == "left") {
      className += 'monster1';
    } else {
      className += 'monster' + rnd(2, 3);
    }
    var m = new Monster(create('div'), className);
    m.position = {"x": x, "y": y};
    m.direction = direction;
    this.monsters.push(m);
  }

  this.map[8][rnd(1, 8)] = 9;
}