function Player(el, className) {
  Player.superclass.constructor.apply(this, arguments);
  this.nextPosition = clone(this.position);
}
extend(Player, Character);

Player.prototype.tryMove = function(direction) {
  this.nextPosition = clone(this.position);
  switch (direction) {
    case "l":
    this.nextPosition.x--;
    break;
    case "r":
    this.nextPosition.x++;
    break;
    case "u":
    this.nextPosition.y--;
    break;
    case "d":
    this.nextPosition.y++;
    break;
  }
}

Player.prototype.move = function() {
  this.position = clone(this.nextPosition);
  return this;
}

