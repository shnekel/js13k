//helpers
function $(id) { return document.getElementById(id);}
function create(el) { return document.createElement(el);}
function rnd(min, max) { return ~~(Math.random() * (max - min + 1)) + min;}

function fillArray(size_x, size_y, filler) {
  var ar = [];
  for (var i=0; i<size_y; i++) {
    ar[i] = [];
    for (var j=0; j<size_x; j++) {
      ar[i][j] = 0;
    }
  }
  return ar;
}

// obj cloning
function clone(obj) {
    if (null === obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}
// inheritance
function extend(Child, Parent) {
  var F = function(){};
  F.prototype = Parent.prototype;
  Child.prototype = new F();
  Child.prototype.constructor = Child;
  Child.superclass = Parent.prototype;
}

// http://jsfiddle.net/alnitak/j8YTe/
function renderPixels(src, done) {
  var img = new Image();

  img.onload = function() {
    var scale = 3;

    var src_canvas = document.createElement('canvas');
    src_canvas.width = this.width;
    src_canvas.height = this.height;

    var src_ctx = src_canvas.getContext('2d');
    src_ctx.drawImage(this, 0, 0);
    var src_data = src_ctx.getImageData(0, 0, this.width, this.height).data;

    var sw = this.width * scale;
    var sh = this.height * scale;

    var dst_canvas = document.createElement('canvas');
    dst_canvas.width = sw;
    dst_canvas.height = sh;
    var dst_ctx = dst_canvas.getContext('2d');

    var dst_imgdata = dst_ctx.createImageData(sw, sh);
    var dst_data = dst_imgdata.data;

    var src_p = 0;
    var dst_p = 0;
    for (var y = 0; y < this.height; ++y) {
      for (var i = 0; i < scale; ++i) {
        for (var x = 0; x < this.width; ++x) {
          var src_p = 4 * (y * this.width + x);
          for (var j = 0; j < scale; ++j) {
            var tmp = src_p;
            dst_data[dst_p++] = src_data[tmp++];
            dst_data[dst_p++] = src_data[tmp++];
            dst_data[dst_p++] = src_data[tmp++];
            dst_data[dst_p++] = src_data[tmp++];
          }
        }
      }
    }
    dst_ctx.putImageData(dst_imgdata, 0, 0);
    done(dst_canvas.toDataURL('image/png'));
  }

  img.src = 'img/' + src + '.png';
}

// init styles
function createCss(css) {
  var style = document.createElement('style')
  style.appendChild(document.createTextNode(css))
  document.head.appendChild(style)
}

(function initCss(){
  var css = '', i, j
  for (i = 0; i < 10; ++i) {
    j = 48 * i
    css += '[data-x="' + i + '"]{left:' + j + 'px}'
    css += '[data-y="' + i + '"]{top:' + j + 'px}'
    css += '[data-monster-x="' + i + '"]{left:' + j + 'px}'
    css += '[data-monster-y="' + i + '"]{top:' + (j - 15) + 'px}'
  }
  createCss(css)

  'player floor2 door wall monster1 monster2 monster3'
  .split(' ').forEach(function (name) {
    renderPixels(name, function (imgData) {
      createCss('.' + name + '{background: url("' + imgData + '")}')
    })
  })
}())
