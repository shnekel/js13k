function Monster(el, className) {
  Monster.superclass.constructor.apply(this, arguments);
  this.direction = "left";
}
extend(Monster, Character);

Monster.prototype.move = function(map) {
  if (this.direction == "right" || this.direction == "left") {
    if (this.position.x == 1) {
      this.direction = "right";
    }
    if (this.position.x == 8) {
      this.direction = "left";
    }
    if (this.direction == "left") {
      this.position.x--;
    } else if (this.direction == "right") {
      this.position.x++;
    }
  }
  if (this.direction == "top" || this.direction == "down") {
    if (this.position.y == 1) {
      this.direction = "down";
    }
    if (this.position.y == 8) {
      this.direction = "top";
    }
    if (this.direction == "top") {
      this.position.y--;
    } else if (this.direction == "down") {
      this.position.y++;
    }
  }
  return this;
}


