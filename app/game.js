(function (){
  SETTINGS = {
    CELL_SIZE: 64,
    MAP_SIZE_X: 19,
    MAP_SIZE_Y: 9
  }

  var levels = 10;
  var currentLevel = 1;
  var tower = [];

  for (var i=0; i<levels; i++) {
    // i for difficulty levels
    tower.push(new Floor(i));
  }

  var game_screen = $('game-screen');
  var splash_screen = $('splash-screen');
  var player = new Player(create('div'), "player");
  var currentLevelMap, currentMonsters = [];
  var inLevel = false;

  function init() {
    game_screen.innerHTML = "";
    currentLevelMap = tower[currentLevel].map;
    currentMonsters = tower[currentLevel].monsters;
    renderLevel();

    player.position = {x: 1, y: 1};
    player.attach(game_screen).render();
    for (var i=0; i<currentMonsters.length; i++) {
      currentMonsters[i].attach(game_screen).render();;
    }
  }

  function startGame() {
    game_screen.style.display = "none";
    splash_screen.style.display = "block";
    splash_screen.onclick = function() {
      game_screen.style.display = "block";
      splash_screen.style.display = "none";
      inLevel = true;
      init();
    }
  }

  startGame();

  function renderLevel() {
    for (var i=0; i<10; i++) {
      for (var j=0; j<10; j++) {
        var el = create('div');
        el.setAttribute('data-x', j);
        el.setAttribute('data-y', i);
        if ( i == 0 || i == 9 || j == 0 || j == 9) {
          el.className = 'tile wall';
        } else  if (currentLevelMap[j][i] === 0) {
          el.className = 'tile floor2';
        } else if (currentLevelMap[j][i] === 9) {
          el.className = 'tile door';
        }
        game_screen.appendChild(el);
      }
    }
  }

  window.onkeydown = function(e) {
    if (!inLevel) {
      return;
    }
    switch (e.keyCode) {
      case 37: // left
      case 65: // A
      if (player.position.x > 1)
        player.tryMove("l");
      break;
      case 38: // up
      case 87: // W
      if (player.position.y > 1)
        player.tryMove("u");
      break;
      case 39: // right
      case 68: // D
      if (player.position.x < 8)
        player.tryMove("r");
      break;
      case 40: // bottom
      case 83: // S
      if (player.position.y < 8)
        player.tryMove("d");
      break;
      default:
      // console.debug("you pressed", e.keyCode);
      break;
    }
    updateLevel();
  }

  function showSplashScreen(msg) {
    game_screen.style.display = "none";
    splash_screen.style.display = "block";
    splash_screen.innerHTML = msg;
    splash_screen.onclick = function() {
      game_screen.style.display = "block";
      splash_screen.style.display = "none";
      inLevel = true;
    }
  }

  function updateLevel() {
    player.move().render();

    for (var i = 0; i<currentMonsters.length; i++) {
      currentMonsters[i].move(currentLevelMap).render();

      if (currentMonsters[i].position.x == player.position.x
        && currentMonsters[i].position.y == player.position.y) {
        showSplashScreen("You're dead! Click to restart")
        inLevel = false;
        currentLevel = 1;
        tower.length = 0;
        for (var j=0; j<levels; j++) {
          // i for difficulty levels
          tower.push(new Floor(j));
        }
        init();
      }
    }

    if (currentLevelMap[player.position.x][player.position.y] === 9) {
      currentLevel++;
      inLevel = false;
      showSplashScreen("Floor " + currentLevel + " click to enter");
      if (currentLevel < 10) {
        init();
      } else {
        inLevel = false;
        showSplashScreen("you win");
      }
    }
  }
}());
