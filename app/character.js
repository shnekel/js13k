// base class for player, monsters and npc
function Character(el, className) {
  this.el = el;
  this.el.className = 'tile ' + className;
  this.position = {
    x: 1,
    y: 1
  }
}

Character.prototype.attach = function(el) {
  el.appendChild(this.el);
  return this;
};

Character.prototype.render = function() {
  this.el.setAttribute('data-monster-x', this.position.x);
  this.el.setAttribute('data-monster-y', this.position.y);
};
